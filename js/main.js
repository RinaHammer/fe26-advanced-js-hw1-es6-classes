/*Теоретичні питання та відповіді.
    1. Поясніть своїми словами, як ви розумієте, як працює прототипне наслідування в Javascript.
    У JavaScript, коли об'єкт потребує доступу до якоїсь властивості чи методу, він спочатку перевіряє свої власні властивості. 
    Якщо він не знаходить там потрібного, то перевіряє властивості свого прототипу, і так далі, вглиб ієрархії прототипів.
   
    2.Для чого потрібно викликати super() у конструкторі класу-нащадка?
    super() викликає конструктор класу-батька, і це потрібно, щоб успадкувати властивості та функціональність класу-батька у клас-нащадок. 
    Таким чином, ми можемо використовувати код з класу-батька в класі-нащадку.


/*Завдання
Створити клас Employee, у якому будуть такі характеристики - name (ім'я), age (вік), salary (зарплата). Зробіть так, щоб ці характеристики заповнювалися під час створення об'єкта.
Створіть гетери та сеттери для цих властивостей.
Створіть клас Programmer, який успадковуватиметься від класу Employee, і який матиме властивість lang (список мов).
Для класу Programmer перезапишіть гетер для властивості salary. Нехай він повертає властивість salary, помножену на 3.
Створіть кілька екземплярів об'єкта Programmer, виведіть їх у консоль.
Примітка
Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Література:
Класи на MDN
Класи в ECMAScript 6*/


// Створюємо клас Employee із властивостями, гетерами та сетерами
class Employee {
    constructor(firstName, age, salary) {
        this._firstName = firstName;
        this._age = age;
        this._salary = salary;
    
}
get firstName() {
    return this._firstName;
}
set firstName(value) {
    this._firstName = value;
}
get age() {
    return this._age;
}
set age(value) {
    this._age = value;
}
get salary() {
    return this._salary;
}
set salary(value) {
    this._salary = value;
}
}

//Створюємо клас Programmer, який успадковує властивості від класу Employee та має властивість lang
class Programmer extends Employee {
    constructor(firstName, age, salary, lang) {
        super(firstName, age, salary);
        this._lang = lang;
    }
    set lang(value){
        this._lang = value;
    }
    get lang() {
        return this._lang;
    }
    get salary() {
        return this._salary * 3;
    }
}

// Створюємо об'єкти класу Programmer
const programmer1 = new Programmer ('Max', 39, 25000, ['Java', 'C++']);
const programmer2 = new Programmer ('Tim', 23, 15000, ['JavaScript', 'TypeScript']);
const programmer3 = new Programmer ('Grace', 85, 85000, ['COBOL', 'FORTRAN', 'FLOW-MATIC', 'UNIVAC I', 'UNIVAC II']);

// Виводимо їх у консоль
console.log('Програміст 1:');
console.log('Ім\'я:', programmer1.firstName);
console.log('Вік:', programmer1.age);
console.log('Зарплата:', programmer1.salary);
console.log('Список мов:', programmer1.lang);

console.log('Програміст 2:');
console.log('Ім\'я:', programmer2.firstName);
console.log('Вік:', programmer2.age);
console.log('Зарплата:', programmer2.salary);
console.log('Список мов:', programmer2.lang);

console.log('Програміст 3:');
console.log('Ім\'я:', programmer3.firstName);
console.log('Вік:', programmer3.age);
console.log('Зарплата:', programmer3.salary);
console.log('Список мов:', programmer3.lang);